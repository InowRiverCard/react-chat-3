package bsa.chat.msgReactions;

import bsa.chat.msgReactions.model.MsgReaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.UUID;

public interface MsgReactionsRepository extends CrudRepository<MsgReaction, UUID> {
    @Query("SELECT r " +
            "FROM MsgReaction r " +
            "WHERE r.user.id = :userId AND r.message.id = :msgId ")
    Optional<MsgReaction> getPostReaction(@Param("userId") UUID userId, @Param("msgId") UUID msgId);
}