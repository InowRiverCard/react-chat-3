package bsa.chat.msgReactions.dto;

import java.util.UUID;

public class MsgReactionDto {
    public UUID messageId;
    public UUID userId;
    public Boolean isLike;
}
