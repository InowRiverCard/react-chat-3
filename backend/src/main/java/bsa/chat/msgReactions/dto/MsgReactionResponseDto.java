package bsa.chat.msgReactions.dto;

import bsa.chat.message.dto.MsgResponseDto;
import bsa.chat.msgReactions.model.MsgReaction;

import java.util.UUID;

public class MsgReactionResponseDto {
    public UUID id;
    public UUID msgId;
    public Boolean isLike;
    public Boolean isReverse;
    public UUID userId;

    public static MsgReactionResponseDto fromEntity(MsgReaction reaction) {
        var result = new MsgReactionResponseDto();
        result.id = reaction.getId();
        result.isLike = reaction.getIsLike();
        result.msgId = reaction.getMessage().getId();
        result.userId = reaction.getUser().getId();
        return result;
    }
}
