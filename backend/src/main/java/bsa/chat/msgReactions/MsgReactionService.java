package bsa.chat.msgReactions;

import bsa.chat.message.MessageNotFoundException;
import bsa.chat.message.MessageRepository;
import bsa.chat.msgReactions.dto.MsgReactionResponseDto;
import bsa.chat.msgReactions.dto.MsgReactionDto;
import bsa.chat.msgReactions.model.MsgReaction;
import bsa.chat.users.UserRepository;
import bsa.chat.users.exceptions.UserNotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class MsgReactionService {
    @Autowired
    private MsgReactionsRepository msgReactionsRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private MessageRepository messageRepository;

    public Optional<MsgReactionResponseDto> setReaction(MsgReactionDto dto) {
        var reaction = msgReactionsRepository.getPostReaction(dto.userId, dto.messageId);

        if (reaction.isPresent()) {
            var react = reaction.get();
            if (react.getIsLike() == dto.isLike) {
                msgReactionsRepository.deleteById(react.getId());
                return Optional.empty();
            } else {
                react.setIsLike(dto.isLike);
                var updated = msgReactionsRepository.save(react);
                var result = MsgReactionResponseDto.fromEntity(updated);
                result.isReverse = true;
                return Optional.of(result);
            }
        } else {
            var msgReaction = new MsgReaction();
            var user = userRepository.findById(dto.userId)
                    .orElseThrow(() -> new UserNotFoundException("id " + dto.userId));
            var message = messageRepository.findById(dto.messageId)
                    .orElseThrow(() -> new MessageNotFoundException("id " + dto.messageId));

            msgReaction.setIsLike(dto.isLike);
            msgReaction.setUser(user);
            msgReaction.setMessage(message);
            var result = msgReactionsRepository.save(msgReaction);
            return Optional.of(MsgReactionResponseDto.fromEntity(result));
        }
    }
}
