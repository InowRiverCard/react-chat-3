package bsa.chat.msgReactions;

import bsa.chat.msgReactions.dto.MsgReactionDto;
import bsa.chat.msgReactions.dto.MsgReactionResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/api/postreaction")
public class MsgReactionController {
    private final MsgReactionService msgReactionService;

    @Autowired
    public MsgReactionController(MsgReactionService msgReactionService) {
        this.msgReactionService = msgReactionService;
    }

    @PutMapping
    public Optional<MsgReactionResponseDto> setReaction(@RequestBody MsgReactionDto dto){
        return msgReactionService.setReaction(dto);
    }

}
