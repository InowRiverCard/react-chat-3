package bsa.chat.message;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 27.07.2020
 */
public class MessageNotFoundException extends RuntimeException {
    public MessageNotFoundException(String msg) {
        super(msg);
    }
}

