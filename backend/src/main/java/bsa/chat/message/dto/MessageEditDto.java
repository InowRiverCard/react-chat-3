package bsa.chat.message.dto;


import java.util.UUID;

public class MessageEditDto {
    public UUID id;
    public String text;

    public MessageEditDto() {
    }

    public MessageEditDto(UUID id, String body) {
        this.id = id;
        this.text = body;
    }
}
