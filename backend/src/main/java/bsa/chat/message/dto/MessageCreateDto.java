package bsa.chat.message.dto;

import java.util.UUID;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 28.07.2020
 */
public class MessageCreateDto {
    public String text;
    public UUID userId;
}
