package bsa.chat.message.dto;

import bsa.chat.message.model.Message;
import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.Date;
import java.util.UUID;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class MessageDetailsDto {
    UUID id;
    String text;
    UUID userId;
    String user;
    Date createdAt;
    Date editedAt;
    String avatar;
    long likeCount;

    public static MessageDetailsDto fromEntity(Message msg) {
        MessageDetailsDto rst = new MessageDetailsDto();
        rst.id = msg.getId();
        rst.text = msg.getText();
        rst.avatar = msg.getUser().getAvatar();
        rst.createdAt = msg.getCreatedAt();
        rst.editedAt = msg.getEditedAt();
        rst.userId = msg.getUser().getId();
        rst.user = msg.getUser().getUsername();
        rst.likeCount = msg.getReactions().size();
        return rst;
    }
}
