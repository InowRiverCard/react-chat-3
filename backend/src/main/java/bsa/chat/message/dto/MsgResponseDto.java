package bsa.chat.message.dto;

import bsa.chat.message.model.Message;
import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.UUID;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class MsgResponseDto {
    UUID id;
    UUID userId;

    public MsgResponseDto() {
    }

    public MsgResponseDto(UUID id, UUID userId) {
        this.id = id;
        this.userId = userId;
    }

    public static MsgResponseDto fromEntity(Message msg) {
        return new MsgResponseDto(msg.getId(), msg.getUser().getId());
    }
}
