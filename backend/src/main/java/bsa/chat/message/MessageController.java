package bsa.chat.message;


import bsa.chat.message.dto.MessageCreateDto;
import bsa.chat.message.dto.MessageDetailsDto;
import bsa.chat.message.dto.MessageEditDto;
import bsa.chat.message.dto.MsgResponseDto;
import bsa.chat.users.exceptions.AuthenticationException;
import bsa.chat.users.exceptions.UserNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping("/api/messages")
public class MessageController {
    @Autowired
    private MessageService msgService;

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({ MessageNotFoundException.class })
    public String handleMsgExceptions(Exception ex) {
        return ex.getMessage();
    }

    @GetMapping
    public List<MessageDetailsDto> getAll() {
        return msgService.getAll();
    }
    
    @GetMapping("/{id}")
    public MessageDetailsDto getById(@PathVariable  UUID id) {
        return msgService.getById(id);
    }

    @PostMapping("/create")
    public MsgResponseDto create(@RequestBody MessageCreateDto dto) {
        return msgService.create(dto);
    }

    @PutMapping("/edit")
    public MsgResponseDto edit(@RequestBody MessageEditDto dto) {
        return msgService.edit(dto);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void delete(@PathVariable  UUID id) {
        msgService.delete(id);
    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({ UserNotFoundException.class, AuthenticationException.class})
    public String handleException(Exception ex) {
        return ex.getMessage();
    }

}
