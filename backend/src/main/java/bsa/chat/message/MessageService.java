package bsa.chat.message;

import bsa.chat.message.dto.MessageCreateDto;
import bsa.chat.message.dto.MessageEditDto;
import bsa.chat.message.dto.MessageDetailsDto;
import bsa.chat.message.dto.MsgResponseDto;
import bsa.chat.message.model.Message;
import bsa.chat.users.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MessageService {
    @Autowired
    private MessageRepository msgRepository;

    @Autowired
    private UserService userService;

    public List<MessageDetailsDto> getAll() {
        return msgRepository
                .findAll()
                .stream()
                .map(MessageDetailsDto::fromEntity)
                .collect(Collectors.toList());
    }

    public MsgResponseDto edit(MessageEditDto msgDto) {
        var message = msgRepository.findById(msgDto.id)
                .orElseThrow(() -> new MessageNotFoundException("id " + msgDto.id));
        message.setText(msgDto.text);
        return MsgResponseDto.fromEntity(msgRepository.save(message));

    }

    public MessageDetailsDto getById(UUID id) {
        var msg = msgRepository.findById(id)
                .orElseThrow(() -> new MessageNotFoundException("id " + id));
        return MessageDetailsDto.fromEntity(msg);
    }

    public MsgResponseDto create(MessageCreateDto msgDto) {
        var user = userService.getById(msgDto.userId);
        var msg = new Message();
        msg.setText(msgDto.text);
        msg.setUser(user);
        return MsgResponseDto.fromEntity(msgRepository.save(msg));
    }

    public void delete(UUID id) {
        msgRepository.deleteById(id);
    }

}
