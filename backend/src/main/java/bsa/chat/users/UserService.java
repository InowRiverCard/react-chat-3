package bsa.chat.users;

import bsa.chat.users.dto.AuthDto;
import bsa.chat.users.dto.UserDetailsDto;
import bsa.chat.users.dto.UserUpsertDto;
import bsa.chat.users.exceptions.AuthenticationException;
import bsa.chat.users.exceptions.UserNotFoundException;
import bsa.chat.users.models.User;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getById(UUID id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new UserNotFoundException("user with such id doesn`t exists " + id.toString()));
    }

    public UserDetailsDto authorise(AuthDto dto) {
        var user = userRepository.findByUsername(dto.name).orElseThrow(AuthenticationException::new);
        if (!user.getPassword().equals(dto.password)) {
            throw new AuthenticationException();
        }
        return  UserDetailsDto.fromEntity(user);
    }

    public UserDetailsDto upsert(UserUpsertDto upsertDto) {
        UserDetailsDto result;
        if (upsertDto.id != null) {
            var user = userRepository
                    .findById(upsertDto.id)
                    .orElseThrow(() -> new UserNotFoundException("user with such id doesn`t exists " + upsertDto.id.toString()));
            user.setEmail(upsertDto.email);
            user.setUsername(upsertDto.name);
            user.setSurname(upsertDto.surname);
            user.setPassword(upsertDto.password);
            user.setRole(upsertDto.role);
            result = UserDetailsDto.fromEntity(userRepository.save(user));
        } else {
            var user = User.fromEntity(upsertDto);
            result = UserDetailsDto.fromEntity(userRepository.save(user));
        }
        return result;
    }

    public void delete(UUID id) {
        userRepository.deleteById(id);
    }

}