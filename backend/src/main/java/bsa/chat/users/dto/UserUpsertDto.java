package bsa.chat.users.dto;

import org.springframework.lang.Nullable;

import java.util.UUID;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 27.07.2020
 */
public class UserUpsertDto {
    @Nullable
    public UUID id;
    public String email;
    public String name;
    public String surname;
    public String password;
    public String role;
}
