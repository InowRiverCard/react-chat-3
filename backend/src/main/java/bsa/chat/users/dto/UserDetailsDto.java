package bsa.chat.users.dto;

import bsa.chat.users.models.User;

import java.util.UUID;

public class UserDetailsDto {
    public UUID id;
    public String email;
    public String name;
    public String surname;
    public String password;
    public String role;

    public static UserDetailsDto fromEntity(User user) {
        var result = new UserDetailsDto();
        result.id = user.getId();
        result.email = user.getEmail();
        result.name = user.getUsername();
        result.surname = user.getSurname();
        result.password = user.getPassword();
        result.role = user.getRole();
        return result;
    }
}
