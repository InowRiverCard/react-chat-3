package bsa.chat.users.dto;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 27.07.2020
 */
public class AuthDto {
    public String name;
    public String password;
}
