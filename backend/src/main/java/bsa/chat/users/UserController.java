package bsa.chat.users;

import bsa.chat.users.dto.AuthDto;
import bsa.chat.users.dto.UserDetailsDto;
import bsa.chat.users.dto.UserUpsertDto;

import bsa.chat.users.exceptions.AuthenticationException;
import bsa.chat.users.exceptions.UserNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@CrossOrigin
@RestController
@RequestMapping("/api/user")
public class UserController {
    private final UserService userService;


    public UserController(UserService userDetailsService) {
        this.userService = userDetailsService;
    }

    @PostMapping("/auth")
    public UserDetailsDto auth(@RequestBody AuthDto dto) {
        return userService.authorise(dto);
    }

    @PostMapping("/upsert")
    public UserDetailsDto upsert(@RequestBody UserUpsertDto dto) {
        return userService.upsert(dto);
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void delete(@PathVariable  UUID id) {
        userService.delete(id);
    }


    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler({ UserNotFoundException.class, AuthenticationException.class})
    public String handleException(Exception ex) {
        return ex.getMessage();
    }
}
