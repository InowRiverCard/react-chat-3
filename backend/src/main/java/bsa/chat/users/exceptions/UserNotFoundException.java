package bsa.chat.users.exceptions;

/**
 * @author Aleksandr Karpachov
 * @version 1.0
 * @since 27.07.2020
 */
public class UserNotFoundException extends RuntimeException{
    public UserNotFoundException(String msg) {
        super(msg);
    }
}
