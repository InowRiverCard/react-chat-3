import {
  createStore,
  applyMiddleware,
  combineReducers
} from 'redux';

import { composeWithDevTools } from 'redux-devtools-extension';
import chatReducer from './container/Chat/reduser';
import profileReducer from './container/Login/reduser';
import messageEditReducer from './container/MessageEdit/reduser'
import createSagaMiddleware from 'redux-saga';
import rootSaga from './sagas'
import { connectRouter, routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';

const initialState = {};

export const history = createBrowserHistory();

const sagaMiddleware = createSagaMiddleware();

const middlewares = [
  sagaMiddleware,
  routerMiddleware(history)
];

const reducers = {
  messages: chatReducer,
  profile: profileReducer,
  editingMessage: messageEditReducer
};

const rootReducer = combineReducers({ 
  router: connectRouter(history),
  ...reducers 
});

const store = createStore(
  rootReducer,
  initialState,
  composeWithDevTools(
    applyMiddleware(...middlewares),
  )
);

sagaMiddleware.run(rootSaga);

export default store;
