import { all } from 'redux-saga/effects';
import chatSaga from '../container/Chat/sagas';
import loginSaga from '../container/Login/sagas';
import msgEditSaga from '../container/MessageEdit/sagas';
// import usersSaga from '../containers/Users/sagas';
// import userPageSaga from '../containers/UserPage/sagas';

export default function* rootSaga() {
  yield all([
    chatSaga(),
    loginSaga(),
    msgEditSaga()
  ])
}
