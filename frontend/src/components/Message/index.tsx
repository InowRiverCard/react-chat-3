import React, { useState } from 'react';
import { Icon, Divider, Popup, Confirm } from 'semantic-ui-react';
import { getUserImgLink } from '../../helpers/imageHelper';
import * as timeConverter from '../../helpers/TimeConverter';
import IMessage from '../../interfaces/IMessage';

import styles from './styles.module.css';

interface MessageProps {
  message: IMessage,
  userId: string,
  isNewDate: boolean,
  deleteMessage(messageId: string): void,
  likeMessage(data: any): void,
  updMessage(id: string): void
}

const Message: React.FC<MessageProps> = ({ message, userId, isNewDate, deleteMessage, likeMessage, updMessage }) => {
  const [isDeleteConfirm, setDeleteConfirm] = useState(false);

  const divider = () => {
    return isNewDate
      ? <Divider horizontal>{timeConverter.getDividerFormatDate(message.createdAt)}</Divider>
      : null;
  }

  const getCurrentUserActions = () => {
    return (
      <div className={styles.myMsgActions}>
        <div className={`${styles.action} ${styles.myAction}`} onClick={() => setDeleteConfirm(true)} >
          <Icon name='trash' /> <span>delete</span>
        </div>
        <div className={`${styles.action} ${styles.myAction}`} onClick={() => updMessage(message.id)} >
          <Icon name='edit' /> <span>edit</span>
        </div>
      </div>
    );
  };

  const isCurrentUser = () => {
    return userId === message.userId;
  } 

  const handleLike = () => {
    const data  = {
      messageId: message.id,
      userId: userId,
      isLike: true
    }
    likeMessage(data);
  }
  
  return (
    <div className={styles.root}>
      {divider()}
      <div className={isCurrentUser() ? `${styles.messageBlock} ${styles.myMsgBlock}` : styles.messageBlock}>
        <img className={styles.avatar} src={getUserImgLink(message.avatar)} alt='ava' />
        <div className={styles.message}>
          <div className={styles.header}>
            <span className={styles.userName}>{message.user}</span>
            <span className={styles.createdAt}>{`created at ${timeConverter.getHours(message.createdAt)}`}</span>
          </div>
          <p className={styles.text}>
            {message.text}
            {message.editedAt ?
              <Popup
                content={timeConverter.getEditFormatDate(message.editedAt)}
                trigger={<span className={styles.edited}>(edited)</span>}
                position='top center'
              />
              : null
            }
          </p>
        </div>
        <div className={styles.actions}>
          {!isCurrentUser() &&
            <div onClick={() => handleLike()}>
              {message.likeCount} {' '} <Icon name='like' />
            </div>
          }
          {isCurrentUser() ? getCurrentUserActions() : null}
        </div>
      </div>
      <Confirm
        size='mini'
        open={isDeleteConfirm}
        onCancel={() => setDeleteConfirm(false)}
        onConfirm={() => deleteMessage(message.id)}
      />
    </div>
  )

}

export default Message;
 