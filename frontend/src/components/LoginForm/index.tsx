import React, { useState } from 'react';
import { Form, Button, Segment } from 'semantic-ui-react';
import IAuth from '../../interfaces/IAuth';

interface LoginFormProps {
  login(auth: IAuth): void,
  setValidationErrors(error: any): void,
  errors: any,
  isLoading: boolean
}

const LoginForm: React.FC<LoginFormProps> = ({ login, setValidationErrors, errors, isLoading }) => {
  const [name, setName] = useState('');
  const [password, setPassword] = useState('');
  
  const nameChangedHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value);
    const updError = ({
      ...errors,
      name: undefined
    });
    setValidationErrors(updError);
  };

  const passwordChangedHandler = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(event.target.value);
    const updError = ({
      ...errors,
      password: undefined
    });
    setValidationErrors(updError);
  };


  const validateName = () => {
    if (!name) {
      const updError = ({
        ...errors,
        ...{ name: 'this field is required' }
      });
      setValidationErrors(updError);
    }
  };

  const validatePwd = () => {
    if (!password) {
      const updError = ({
        ...errors,
        ...{ password: 'this field is required'}
      });
      setValidationErrors(updError);
    }
  };

  const validateInputData = () => {
    validateName();
    validatePwd();
  };

  const handleLoginClick = () => {
    validateInputData();
    const isValid = !errors.name && !errors.password;
    if (!isValid || isLoading) {
      return;
    }
    login({ name, password });
  };

  return (
    <Form name="loginForm" size="large" onSubmit={handleLoginClick}>
      <Segment>
        <Form.Input
          fluid
          icon="user circle"
          iconPosition="left"
          placeholder="Name"
          type="text"
          error={errors.name}
          onChange={nameChangedHandler}
          onBlur={validateName}
        />
        <Form.Input
          fluid
          icon="lock"
          iconPosition="left"
          placeholder="Password"
          type="password"
          error={errors.password}
          onChange={passwordChangedHandler}
          onBlur={validatePwd}
        />
        <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
          Login
        </Button>
      </Segment>
    </Form>
  );
};

export default LoginForm;
