import React from 'react'
import { Loader } from 'semantic-ui-react'
import Message from '../Message';
import IMessage from '../../interfaces/IMessage';
import IUser from '../../interfaces/IUser';

import styles from './styles.module.css';

interface MessageBoxProps {
  isLoading: boolean | undefined,
  messages: IMessage[],
  getDate(date: string): string,
  deleteMessage(messageId: string): void,
  user: IUser,
  likeMessage(messegeId: string): void,
  setEditMsg(id: string): void
}

class MassageBox extends React.Component<MessageBoxProps> {
  private prevDate: string;
  private prevHeight: number;
  private messageList: HTMLDivElement | null;

  constructor(props: MessageBoxProps) {
    super(props);
    this.prevDate = '';
    this.prevHeight =  0;
    this.messageList = null;

    this.scrollToBottom = this.scrollToBottom.bind(this);
    this.getDate = this.getDate.bind(this);
    this.isTheSameDate = this.isTheSameDate.bind(this);
    this.updatePrevDate = this.updatePrevDate.bind(this);
    this.getMassegeList = this.getMassegeList.bind(this);
  }

  scrollToBottom() {
    const scrollHeight = this.messageList!.scrollHeight;
    const height = this.messageList!.clientHeight;
    if (this.prevHeight !== scrollHeight) {
      this.prevHeight = scrollHeight;
      const maxScrollTop = scrollHeight - height;
      this.messageList!.scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
    }
  }

  componentDidUpdate() {
    this.scrollToBottom();
    this.prevDate = '';
  }

  isTheSameDate(currentDate: string) {
    const formattedDate = this.getDate(currentDate);
    return this.prevDate === formattedDate;
  }

  updatePrevDate(newDate: string) {
    const formattedDate = this.getDate(newDate);
    this.prevDate = formattedDate;
  }

  getDate(date: string) {
    return this.props.getDate(date);
  }

  getMassegeList(messages: IMessage[]) {
    if (!messages || messages.length === 0) {
      return null;
    }

    const msgMap = (msg: IMessage) => {
      const isNewDate = !this.isTheSameDate(msg.createdAt)
      const userId = this.props.user.id;
      if (isNewDate) {
        this.updatePrevDate(msg.createdAt)
      }

      return <Message
        key={msg.id}
        message={msg}
        userId={userId}
        isNewDate={isNewDate}
        updMessage={this.props.setEditMsg}
        deleteMessage={this.props.deleteMessage}
        likeMessage={this.props.likeMessage}
      />
    }
    return messages.map(msg => msgMap(msg));
  };

  render() {
    return (
      <div className={`${styles.messages} segment`} ref={(div) => { this.messageList = div; }}>
        {this.props.isLoading
          ? <Loader content='Loading' active />
          : this.getMassegeList(this.props.messages)
        }
      </div>
    )
  }
};

export default MassageBox;
