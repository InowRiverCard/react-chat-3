
import React from 'react';
import Login from './container/Login';
import Chat from './container/Chat';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import UserList from './container/UserTable';
import EditUser from './container/UserEdit';
import EditMessage from './container/MessageEdit';
import { Switch } from 'react-router-dom';
import PrivateRoute from './container/PrivateRoute';
import PublicRoute from './container/PublicRoute';

const App = ({ isAuthorized, isAdmin }) => {
  return (
    <Switch>
      <PublicRoute exact path='/login' component={Login} />
      <PrivateRoute path='/edit_msg/:id' component={EditMessage} access={isAuthorized} />
      <PrivateRoute exact path="/edit_user" component={EditUser} access={isAdmin} />
      <PrivateRoute path="/edit_user/:id" component={EditUser} access={isAdmin} />
      <PrivateRoute exact path='/user_list' component={UserList} access={isAdmin} />
      <PrivateRoute exact path='/' component={Chat} access={isAuthorized} />
    </Switch>
  );
}

App.propTypes = {
  isAuthorized: PropTypes.bool.isRequired,
  isAdmin: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({
  isAuthorized: state.profile.isAuthorized,
  isAdmin: state.profile.isAdmin
});


export default connect(mapStateToProps, null)(App);