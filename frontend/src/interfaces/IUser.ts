export default interface IUser {
  id: string,
  name : string,
  surname: string,
  email: string,
  avatar : string,
  role: string
}