export default interface IMessage {
  id: string,
  text: string,
  user: string, 
  avatar: string, 
  userId: string, 
  editedAt: string,
  createdAt: string,
  likeCount: number
}