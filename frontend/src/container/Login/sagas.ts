import axios from 'axios';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { push } from 'connected-react-router'
import { LOGIN } from './actionTypes';
import { setIsLoading, setUserAction, setIsAuthorized, setIsAdmin } from './actions';
import api from '../../shared/config/api.json';


function* login(action: any) {
  // const id = action.auth;
  try {
    yield put(setIsLoading(true));
    const user = yield call(axios.post, `${api.url}/user/auth`, action.auth);
    yield put(setUserAction(user.data));
    yield put(setIsAuthorized(true));
    const isAdmin = user.data.role === 'admin';
    yield put(setIsAdmin(isAdmin));
    if (isAdmin) {
      yield put(push('/user_list'))
    }
  } catch (error) {
    console.log('auth err ', error.message);
  } finally{
    yield put(setIsLoading(false));
  }
}

function* watchLogin() {
  yield takeEvery(LOGIN, login)
}

export default function* loginSaga() {
  yield all([
    watchLogin()
  ])
}

//function* setErrors(action: any)