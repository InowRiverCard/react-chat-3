import {
  SET_USER,
  LOGIN,
  SET_VALIDATION_ERRORS,
  SET_IS_LOADING,
  SET_IS_ADMIN,
  SET_IS_AUTHORIZED
} from './actionTypes';

export const setUserAction = (user) => ({
  type: SET_USER,
  user
});

export const login = (auth) => ({
  type: LOGIN,
  auth
});

export const setValidationErrors = (errors) => ({
  type: SET_VALIDATION_ERRORS,
  errors
});

export const setIsLoading = (isLoading) => ({
  type: SET_IS_LOADING,
  isLoading
});

export const setIsAuthorized = (isAuthorized) => ({
  type: SET_IS_AUTHORIZED,
  isAuthorized
});

export const setIsAdmin = (isAdmin) => ({
  type: SET_IS_ADMIN,
  isAdmin
});