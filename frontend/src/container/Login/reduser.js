import {
  SET_USER,
  SET_VALIDATION_ERRORS,
  SET_IS_LOADING,
  SET_IS_ADMIN,
  SET_IS_AUTHORIZED
} from './actionTypes';


export default (state = {}, action) => {
  switch (action.type) {
    case SET_USER:
      return {
        ...state,
        user: action.user,
      };
    case SET_VALIDATION_ERRORS:
      return {
        ...state,
        errors: action.errors,
      };
    case SET_IS_LOADING:
      return {
        ...state,
        isLoading: action.isLoading,
      };
    case SET_IS_ADMIN:
      return {
        ...state,
        isAdmin: action.isAdmin,
      };
    case SET_IS_AUTHORIZED:
      return {
        ...state,
        isAuthorized: action.isAuthorized,
      };
    default:
      return state;
  }
};