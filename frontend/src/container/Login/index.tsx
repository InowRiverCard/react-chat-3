/* eslint-disable no-unused-vars */
import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { login, setValidationErrors } from '../Login/actions';
import LoginForm from '../../components/LoginForm';
import { Grid, Header } from 'semantic-ui-react';
import IAuth from '../../interfaces/IAuth';


interface LoginPageProps {
  login(auth: IAuth): void,
  setValidationErrors(error: any): void,
  validationErrors: any,
  isLoding: boolean
}

const LoginPage: React.FC<LoginPageProps> = ({ login, setValidationErrors, validationErrors, isLoding }) => {
  return (
    <Grid textAlign="center" verticalAlign="middle" className="fill" >
      <Grid.Column style={{ maxWidth: 450 }}>
        <Header as="h2" color="teal" textAlign="center">
          <span>Login to your account</span>
        </Header>
        <LoginForm
          login={login}
          setValidationErrors={setValidationErrors}
          errors={validationErrors}
          isLoading={isLoding}
        />
      </Grid.Column>
    </Grid>
  );
};

LoginPage.propTypes = {
  login: PropTypes.func.isRequired,
  setValidationErrors: PropTypes.func.isRequired,
  validationErrors: PropTypes.objectOf(PropTypes.any),
}

LoginPage.defaultProps = {
  validationErrors: {},
};

const mapStateToProps = (rootState: any) => ({
  validationErrors: rootState.profile.errors,
  isLoading: rootState.profile.validationErrors
});

const actions = { login, setValidationErrors };

export default connect(
  mapStateToProps,
  actions
)(LoginPage);
