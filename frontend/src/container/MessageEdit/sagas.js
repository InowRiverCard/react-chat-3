import axios from 'axios';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import { fetchMessageAction, setMessageAction } from './actions';
import api from '../../shared/config/api';

function* setMessage(action) {
  try {
    const end = action.payload.id;
    const message = yield call(axios.get, `${api.url}/messages/${end}`);
    console.log(message);
    yield put(setMessageAction(message.data))
  } catch (error) {
    console.log('setMsg error:', error.message)
  }
}

function* watchSetAllMessages() {
  yield takeEvery(fetchMessageAction, setMessage);
}

export default function* msgEditSaga() {
  yield all([
    watchSetAllMessages()
  ])
}