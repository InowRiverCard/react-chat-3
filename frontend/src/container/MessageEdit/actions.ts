import {
  FETCH_MESSAGE, FETCH_MESSAGE_SUCCESS
} from './actionTypes';
import IMessage from '../../interfaces/IMessage';

export const fetchMessageAction = (id: string) => ({
  type: FETCH_MESSAGE,
  payload: { id }
});

export const setMessageAction = (message: IMessage) => ({
  type: FETCH_MESSAGE_SUCCESS,
  message
});

