import {
  FETCH_MESSAGE_SUCCESS
} from './actionTypes';
  
const initState = {
  message: {id: "" , text: ""},
};

export default (state = initState, action) => {
  switch (action.type) {
    case FETCH_MESSAGE_SUCCESS:
      return {
        ...state,
        message: action.message,
      };
    default:
      return state;
  }
};