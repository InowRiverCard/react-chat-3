import React from 'react';
import { connect } from 'react-redux';
import { Segment, Button } from 'semantic-ui-react';
import { Picker, Emoji } from 'emoji-mart';
import defaultMsgData from '../../shared/config/defaultMsgConfig.json';
import { fetchMessageAction } from './actions'

import {
  editMessage
} from '../Chat/actions'
import styles from './styles.module.css';



class MessageEdit extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      message: this.getDefaultMsgData(),
      piker: false
    }
    this.editMessage = this.editMessage.bind(this);
    this.handleSave = this.handleSave.bind(this);
    this.toglePicker = this.toglePicker.bind(this);
    this.changeHandler = this.changeHandler.bind(this);
    this.keyPressHandler = this.keyPressHandler.bind(this);
    this.addEmoji = this.addEmoji.bind(this);
    this.close = this.close.bind(this);
    
  }

  getDefaultMsgData() {
    return {...defaultMsgData}
  }

  static getDerivedStateFromProps(nextProps, prevState) {
    if (nextProps.message.id !== prevState.message.id) {
      return {
        message: nextProps.message
      };
    } else {
      return null;
    }
  }

  componentDidMount() {
    this.props.fetchMessageAction(this.props.match.params.id)
  }

  editMessage(message) {
    this.props.editMessage(message);
  }

  handleSave() {
    if (!this.state.message.text) {
      return;
    }
    this.editMessage({id: this.props.message.id, text: this.state.message.text })
    this.close();
  };

  changeHandler(event) {
    this.setState({ message: {...this.state.message, text: event.target.value }});
  }

  keyPressHandler(event) {
    if (event.key === 'Enter' && this.state.text.trim() !== '') {
      this.handleSave();
    }
  }

  addEmoji(e) {
    this.setState({ message: { ...this.state.message, text: `${this.state.message.text}${e.native}` } });
  }

  toglePicker()  {
    this.setState({ ...this.state, piker: !this.state.piker });
  }

  close() {
    this.setState({
      message: this.getDefaultMsgData(),
      piker: false
    })
    this.props.history.push('/');
   
  }

  render() {
    return (
      <div className={styles.container} >
        <Segment>
          <span>Edit Message</span>
        </Segment>
        <Segment>
          <div className={styles.content} >
            <textarea
              rows={10}
              value={this.state.message.text}
              placeholder="Enter message"
              onChange={this.changeHandler}
              onKeyPress={this.keyPressHandler}
            />
            <div className={styles.smileBtn} onClick={this.toglePicker}>
              <Emoji emoji=":smiley:" size={16} />
            </div>
          </div>
          {this.state.piker &&
            <Picker
              native
              style={{ position: "absolute" }}
              onSelect={this.addEmoji} showSkinTones={false}
              emojiSize={16}
              showPreview={false}
            />
          }
        </Segment>
        <div>
          <Button
            className={styles.btn}
            icon="save outline"
            floated="right"
            color="blue"
            onClick={this.handleSave}
            type="submit"
            disabled={!this.state.message.text}
            content="Save"
          />
          <Button className={styles.btn} floated="right" onClick={() => this.close()} basic>Cancel</Button>
        </div>
      </div >
    );

  }
}

const mapStateToProps = rootState => {
  return {
    message: rootState.editingMessage.message,
    user: rootState.profile.user
  } 
};

const actions = {
  fetchMessageAction,
  editMessage
};

export default connect(
  mapStateToProps,
  actions
)(MessageEdit);