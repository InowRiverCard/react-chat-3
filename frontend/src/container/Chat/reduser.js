import {
  SET_ALL_MESSAGES_SUCCESS,
  SET_IS_LOADING
} from './actionTypes';
  
const initState = {
  messages: [],
  isLoading: false
};

export default (state = initState, action) => {
  switch (action.type) {
    case SET_ALL_MESSAGES_SUCCESS:
      return {
        ...state,
        messages: action.messages,
      };
    case SET_IS_LOADING:
      return {
        ...state,
        isLoading: action.isLoading
      };
    default:
      return state;
  }
};