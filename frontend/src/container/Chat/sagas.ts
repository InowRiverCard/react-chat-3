import axios from 'axios';
import { call, put, takeEvery, all } from 'redux-saga/effects';
import {
  SET_ALL_MESSAGES,
  ADD_MESSAGE,
  EDIT_MESSAGE,
  DELETE_MESSAGE,
  LIKE_MESSAGE
} from './actionTypes';
import api from '../../shared/config/api.json';

import { loadMessages, setIsLoading, setMessages } from './actions';


function* setAllMessages() {
  try {
    yield put(setIsLoading(true));
    const messages = yield call(axios.get, `${api.url}/messages`);
    yield put(setMessages(messages.data))
  } catch (error) {
    console.log('setAllMessages error:', error.message)
  } finally {
    yield put(setIsLoading(false));
  }
}



function* watchSetAllMessages() {
  yield takeEvery(SET_ALL_MESSAGES, setAllMessages);
}

function* addMessage(action: any) {
  try {
    yield call(axios.post, `${api.url}/messages/create`, action.message);
    yield put(loadMessages());
  } catch (error) {
    console.log('createMessage error:', error.message);
  }
}

function* watchAddMessage() {
  yield takeEvery(ADD_MESSAGE, addMessage)
}

function* editMessage(action: any) {
  try {
    yield call(axios.put, `${api.url}/messages/edit`, action.message);
    yield put(loadMessages());
  } catch (error) {
    console.log('updateMessage error:', error.message);
  }
}

function* watchUpdateMessage() {
  yield takeEvery(EDIT_MESSAGE, editMessage)
}

function* deleteMessage(action: any) {
  try {
    yield call(axios.delete, `${api.url}/messages/${action.id}`);
    yield put(loadMessages())
  } catch (error) {
    console.log('deleteMessage Error:', error.message);
  }
}

function* watchDeleteMessage() {
  yield takeEvery(DELETE_MESSAGE, deleteMessage)
}

function* likeMsg(action: any) {
  try {
    yield call(axios.put, `${api.url}/postreaction`, action.data);
    yield put(loadMessages());
  } catch (error) {
    console.log('like error:', error.message);
  }
}

function* watchLikeMessage() {
  yield takeEvery(LIKE_MESSAGE, likeMsg)
}

export default function* chatSaga() {
  yield all([
    watchSetAllMessages(),
    watchAddMessage(),
    watchUpdateMessage(),
    watchDeleteMessage(),
    watchLikeMessage()
  ])
}