import {
  SET_ALL_MESSAGES,
  SET_ALL_MESSAGES_SUCCESS,
  ADD_MESSAGE,
  SET_IS_LOADING,
  DELETE_MESSAGE,
  EDIT_MESSAGE,
  LIKE_MESSAGE
} from './actionTypes';

export const setMessages = messages => ({
  type: SET_ALL_MESSAGES_SUCCESS,
  messages
});

export const addMessage = message => ({
  type: ADD_MESSAGE,
  message
});

export const setIsLoading = isLoading => ({
  type: SET_IS_LOADING,
  isLoading
});

export const deleteMessage = id => ({
  type: DELETE_MESSAGE,
  id
});

export const editMessage = message => ({
  type: EDIT_MESSAGE,
  message
});

export const loadMessages = () => ({
  type: SET_ALL_MESSAGES
});

export const likeMessage = (data) => ({
  type: LIKE_MESSAGE,
  data
});

