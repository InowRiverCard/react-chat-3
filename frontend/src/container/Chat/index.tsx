
import React from 'react';
import InputBlock from '../../components/InputBlock';
import ChatHeader from '../../components/Header';
import MessageBox from '../../components/MessageBox';
import * as timeConverter from '../../helpers/TimeConverter';
import { connect } from 'react-redux';
import IMessage from '../../interfaces/IMessage';
import IUser from '../../interfaces/IUser';
import {
  addMessage,
  editMessage,
  deleteMessage,
  setIsLoading,
  loadMessages,
  likeMessage
}  from './actions'

import styles from './styles.module.css';

interface ChatProps {
  messages: IMessage[],
  user: IUser,
  editedMsg: IMessage | undefined,
  isLoading: boolean | undefined,
  setIsLoading(isLoading: boolean): void,
  loadMessages(): void,
  addMessage(message: IMessage): void,
  editMessage(message: IMessage): void,
  deleteMessage(MsgId: string): void,
  likeMessage(data: any): void,
  history: any
}

class Chat extends React.Component<ChatProps> {
  constructor(props: ChatProps) {
    super(props);
    this.getParticipantCount = this.getParticipantCount.bind(this);
    this.addMessage = this.addMessage.bind(this);
    this.updateMessage = this.updateMessage.bind(this);
    this.deleteMessage = this.deleteMessage.bind(this);
    this.likeMsg = this.likeMsg.bind(this);
    this.getLastMessageDate = this.getLastMessageDate.bind(this);
    this.getLastMessage = this.getLastMessage.bind(this);
  }

  componentDidMount() {
    this.props.loadMessages() 
  }

  addMessage(newMessage: IMessage) {
    this.props.addMessage(newMessage);
  }

  updateMessage(id: string) {
    this.props.history.push(`/edit_msg/${id}`);
  }

  deleteMessage(MsgId: string) {
    this.props.deleteMessage(MsgId);
  }

  likeMsg(data: any) {
    this.props.likeMessage(data);
  }

  getParticipantCount() {
    var unicUsers = new Set<string>();
    this.props.messages.map(msg => unicUsers.add(msg.user));
    return unicUsers.size;
  }

  getLastMessageDate() {
    const messages = this.props.messages;
    if (messages.length === 0) {
      return 'never';
    }
    const lastMsg = messages[messages.length - 1];
    return timeConverter.getDividerFormatDate(lastMsg.createdAt);
  }

  getLastMessage(): IMessage | null {
    const messages = this.props.messages;
    if (messages.length === 0) {
      return null;
    }
    const lastMsg = messages[messages.length - 1];
    return lastMsg.userId === this.props.user.id ? lastMsg : null;
  }

  render() {
    return (
      <div className={styles.application}>
        <ChatHeader
          participants={this.getParticipantCount()}
          messages={this.props.messages!.length}
          mostRecentDate={this.getLastMessageDate()}
        />
        <MessageBox
          isLoading={this.props.isLoading}
          messages={this.props.messages}
          getDate={timeConverter.getDateWithoutTime}
          deleteMessage={this.deleteMessage}
          user={this.props.user}
          likeMessage={this.likeMsg}
          setEditMsg={this.updateMessage}
        />
        <InputBlock
          user={this.props.user}
          sendMessage={this.addMessage}
          LastMessage={this.getLastMessage()}
          setEditMsg={this.updateMessage}
        />
      </div>
    );
  }
}


const mapStateToProps = (rootState: any) => ({
  messages: rootState.messages.messages,
  user: rootState.profile.user,
  editedMsg: rootState.messages.editedMsg,
  isLoading: rootState.messages.isLoading
});

const actions = {
  addMessage,
  editMessage,
  deleteMessage,
  setIsLoading,
  loadMessages,
  likeMessage
};

//const mapDispatchToProps = (dispatch: any) => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  actions
)(Chat);
